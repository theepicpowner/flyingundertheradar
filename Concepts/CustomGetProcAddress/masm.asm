; developed and tested on:
;Edition    Windows Server 2022 Datacenter
;Version    21H2
;OS build   20348.2031
;AMDx64

; Offsets may be different on other builds - use Windbg to confirm offsets if bugs

; sources:
; https://xen0vas.github.io/Win32-Reverse-Shell-Shellcode-part-2-Locate-the-Export-Directory-Table/
; https://www.linkedin.com/posts/john-stigerwalt-90a9b4110_assemblyprogramming-cplusplus-nasm-activity-7171882426560856064-1noU?utm_source=share&utm_medium=member_desktop
; http:;www.rohitab.com/discuss/topic/45310-obtain-dll-addresses-from-the-peb-of-a-64-bit-process/

.code
ALIGN 16

; example of direct resolve of ntdll.dll base
GetNtdllBase proc
    ; Load the address of PEB into RAX via GS segment
    xor rax, rax
    mov rax, gs:[60h]  

    ; Navigating PEB->Ldr->InLoadOrderModuleList
    mov rax, [rax + 18h]         ; Offset of PEB->Ldr in PE
    mov rax, [rax + 20h]         ; Offset of Ldr->InLoadOrderModuleList 
    ; Ldr->InLoadOrderModuleList points to the first LDR_DATA_TABLE_ENTRY structure in the linked list

    ; We are currently at linked list node 0 (PE itself) so we can derefence once to get to node 1 (ntdll.dll)
    mov rax, [rax] 
    ; Since we are in LDR_DATA_TABLE_ENTRY and already at offset 0x10 (InMemoryOrderLinks)
    ; We add 0x20 to access the dllBase address (offset 0x30) value by derefencing
    mov rax, [rax + 20h]     

    ; We now have NTDLL.DLL base address in rax which we can return
    ; or use for further assembly operations
    ret 
GetNtdllBase endp

; custom GPA in assembly
GetBase proc

    ; Input parameters:
    ;   rcx: Pointer to target dllName string
    ;   rdx: Length of the target dllName string
    ; Output:
    ;   rax: Length of the string

    ; Important variables
    mov r12, rcx ; r12 contains ptr to target dllName str
    mov r13, rdx ; r13 contains value of target dllName length
   
    ; clear these registers
    xor rcx, rcx
    xor rdx, rdx

     ; Load the address of PEB into RAX via GS segment
    xor rax, rax
    mov rax, gs:[60h]  

    ; Navigating PEB->Ldr->InLoadOrderModuleList
    mov rax, [rax + 18h]         ; Offset of PEB->Ldr in PE
    mov rax, [rax + 20h]         ; Offset of Ldr->InLoadOrderModuleList 
 
    ; Loop through InLoadOrderModuleList double-linked list module entries

module_loop:
    ; Check if current pointer of rax is null 
	; If null, end of linkedlist has been reached and module has not been found
    cmp qword ptr [rax], 0
    je base_error

	; Get the entry in the InLoadOrderModuleList (ntdll.dll)
    mov rax, [rax]               
    ; Current offset is 0x10 already
	; As we are in the linked list element of the LDR_DATA_TABLE_ENTRY struct
    
	; baseDLLName should be at 0x58-0x10 (offset 0x48)
	; DllBaseName struct has Length at 0x00 
    ; Length of the module name struct at [rax + 48h]
    ; Divide length by 2 to account for null byte seperators
    xor rdx, rdx
    mov dx, word ptr [rax + 48h] ; length of module name 
    ; length of string with every char suceeded by a NULL byte so twice the length of number of characters
    ; we need to divide the length by 2, to get the number of characters
    shr rdx, 1 

	; Compare current module baseDLLName with target dllName length 
    cmp rdx, r13
    ; if length not equal move to next item
    jnz module_loop 
    ; if equal move to byte-by-byte comparison of the two strings

	; Compare each byte in current module DllBaseName string with our target dllName string
    ; If strings match, find the base address of current module    

    ; rbx contains module string length
    mov rcx, [rax + 48h + 8h] ; access current module string pointer as arg1
    mov rdx, r12 ; target module name as arg2
    mov r8, r13  ; length as arg3

    ;save rax for later if needed
    push rax

    ; need a string comparisson function that handles the char offsets of module name (every other byte)
    ; rcx - module name ptr
    ; rdx - target module name
    ; r8 - common real length

    call compare_module_strings ; compare strings that handles the null bytes

    ; check if rax is 0; if not strings not equal
    cmp rax, 0
    ; restore registers
    pop rax
    jnz module_loop ; if no match continue looping 

    ; rax contains ptr to current module struct LDR_DATA_TABLE_ENTRY
	; get the offset of the current module baseAddress at 0x30
	; Since we are already at offset 0x10 in LDR_DATA_TABLE_ENTRY
	; Offset to base address is (0x30-0x10) so 0x20
    mov rax, [rax + 20h] 
    ret

base_error:
	; return 0 if error
    mov rax, 0 
    ret

GetBase endp

; module name search func
; rcx will be ptr to module name that is null byte character seperated (need to skip a byte for each comparison)
compare_module_strings PROC
    ; Inputs:
    ;   rcx: Pointer to the first string  ; current module name (need to iterate over every other byte)
    ;   rdx: Pointer to the second string ; target module name 
    ;   r8: Length of the strings
    ; Outputs:
    ;   rax: Zero if strings are equal, non-zero otherwise

    ; Compare byte by byte
    ; use r14 and r15 since free
    xor r14, r14
    xor r15, r15
    ; rcx is default loop counter
    mov rbx, rcx ; put rcx str into rsi
    mov rcx, r8 ; put length in rcx

compare_loop:
    mov r14b, BYTE PTR [rbx]  ; Load byte from the first string
    mov r15b, BYTE PTR [rdx]  ; Load byte from the second string ;rdx
    cmp r14b, r15b              ; Compare bytes
    jne strings_not_equal   ; If bytes are not equal, jump to strings_not_equal
    ; for every iteration rbx needs to interate twice
    inc rbx                 ; Move to the next byte in the first string
    inc rbx
    inc rdx                 ; Move to the next byte in the second string ; rdx
    loop compare_loop       ; Loop until all bytes are compared

    ; If loop completes without finding a difference, strings are equal
    mov rax, 0  ; Set return value to 0 (strings are equal)
    jmp end_compare_strings

; if not equal restore registers from stack and loop to next currentFunc?
strings_not_equal:
    ; If a difference is found, set return value to non-zero
    mov rax, 1  ; Set return value to 1 (strings are not equal)

end_compare_strings:
    ret

compare_module_strings ENDP

END
