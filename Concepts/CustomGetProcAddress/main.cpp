#include <Windows.h>
#include <stdio.h>

// Assembly function declarations
extern "C" DWORD_PTR GetBase(LPCSTR dllName, SIZE_T length);
extern "C" int get_string_length(char* i);// works
extern "C" void* Obf(char* i, size_t l);
extern "C" void* DeObf(char* i, size_t l);
extern "C" int main();

int main() {

    // SEARCH STRINGS
    char dllNameObf[] = "osekm-ekm"; // ntdll.dll       
    char* dllName = { 0 }; // ASM WILL HANDLE UPPERCASE AND LOWERCASE    
    dllName = (char*)DeObf(dllNameObf, get_string_length(dllNameObf));
    
    // Functional x64 MASM assembly custom GetProcAddress    
    // Resolves base address of any process loaded DLL
    // PARAMS: DllName, DllName length 
    size_t nameLen = strlen(dllName);     
    DWORD_PTR baseAddress = GetBase(dllName, nameLen); // store retrieved address
    if (baseAddress == 0) {
        printf("Failed to get %s base address \n", dllName);
        return 1;        
    }
    printf(" %s base address (Assembly DLL resolve via PEB): 0x%p \n", dllName, baseAddress);

    // getchar(); // effectively pause EXE by getting user input
    return 0;
}
