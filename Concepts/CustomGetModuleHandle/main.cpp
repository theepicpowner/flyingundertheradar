#include <Windows.h>
#include <stdio.h>

// Assembly function declarations
//extern "C" DWORD_PTR GetNtdllBase();
extern "C" DWORD_PTR GetBase(LPCSTR dllName, SIZE_T length);
extern "C" DWORD_PTR resolveFunction(LPCSTR functionName, SIZE_T length, DWORD_PTR moduleBase);
extern "C" int get_string_length(char* i);
extern "C" void* Obf(char* i, size_t l);
extern "C" void* DeObf(char* i, size_t l);
extern "C" int main();

int main() {
   
    // SEARCH STRINGS
    char dllNameObf[] = "osekm-ekm"; // ntdll.dll obfuscated with assembly
    char functionNameObf[] = "OsEdm`zDyddtuhpm"; // NtDelayExecution obfuscated with assembly
  
    char* dllName = { 0 }; // GetBase will handle UPPERCASE or lowercase dllName strings    
    char* functionName = {0}; // Function names are case sensitive

    dllName = (char*)DeObf(dllNameObf, get_string_length(dllNameObf));
    functionName = (char*)DeObf(functionNameObf, get_string_length(functionNameObf));

    // Functional x64 MASM assembly custom GetProcAddress    
    // Resolves base address of any process loaded DLL
    // PARAMS: DllName, DllName length 
    size_t nameLen = get_string_length(dllName);
    DWORD_PTR baseAddress = GetBase(dllName, nameLen); // store retrieved address
    if (baseAddress == 0) {        
        printf("Failed to get base. \n");
        return 1;
    }
    printf(" %s base address (Assembly DLL resolve via PEB): 0x%p \n", dllName, baseAddress);

    // Functional x64 MASM assembly custom GetModuleHandle
    // Resolves address of any process loaded DLL function 
    // PARAMS: FuncName, FuncName length, DLL base address   
    size_t length = get_string_length(functionName);
    DWORD_PTR functionAddress = resolveFunction(functionName, length, baseAddress);
    if (functionAddress == 0) {
        printf("Failed to get func address. \n");
        return 1;
    }
    printf(" %s function address (Assembly NtFunc resolve via PEB): 0x%p \n", functionName, functionAddress);

    //getchar();
    return 0;
}
