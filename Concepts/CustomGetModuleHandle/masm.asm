; developed and tested on:
;Edition	Windows Server 2022 Datacenter
;Version	21H2
;OS build	20348.2031
;AMDx64

; Offsets may be different on other Windows builds - use Windbg to confirm offsets if execution bugs

; sources:
; https://xen0vas.github.io/Win32-Reverse-Shell-Shellcode-part-2-Locate-the-Export-Directory-Table/
; https://www.linkedin.com/posts/john-stigerwalt-90a9b4110_assemblyprogramming-cplusplus-nasm-activity-7171882426560856064-1noU?utm_source=share&utm_medium=member_desktop
; http:;www.rohitab.com/discuss/topic/45310-obtain-dll-addresses-from-the-peb-of-a-64-bit-process/

.data
; strings here if needed

.code
ALIGN 16

; essentually custom GPA in assembly
GetBase proc

    ; Input parameters:
    ;   rcx: Pointer to dll base string
    ;   rdx: Length of the string
    ; Output:
    ;   rax: Length of the string

    ; mov rax to r13 for comparison
    mov r12, rcx ; dllName
    mov r13, rdx ; length
   
    xor rcx, rcx
    xor rdx, rdx


    ; Load the address of PEB into RAX
    xor rax, rax
    mov rax, gs:[60h]  ;

    ; Retrieve the base address of ntdll.dll from the PEB
    mov rax, [rax + 18h]           ; Offset of Ldr in PE
    mov rax, [rax + 20h]         ; Offset of InLoadOrderModuleList in Ldr ; 0x20 to be used,
    ; + 0x10 seems to also points to NTDLL.dll via other list

    ; make a loop here that derefs and checks if its the one


module_loop:
    ; check if current pointer of rax is null ; if so end of linkedlist?
    cmp qword ptr [rax], 0
    je base_error

    mov rax, [rax]                ; Get the first entry in the InLoadOrderModuleList (ntdll.dll)
    ; this is offset 0x10 already as its the ..Links element in the struct _LDR_DATA_TABLE_ENTRY
    ; baseDLLName should be at 0x58 (minus 0x10) - so in theory 0x48
    ; get the baseDLLName at offset 0x48
    ; length of the module name struct at [rax + 48h] just compare that to our string len!

    ; cmp module name length to our targetmodule string length
    xor rdx, rdx
    mov dx, word ptr [rax + 48h] ; length of module name (this is length of string with every char suceeded by a null byte so 2x as long)
    ; we need to divide by 2 - this can be done with shr
    shr rdx, 1 ; devide by 2
    ; this number therefore needs to be halved (this would then be the amount of chars without the final null byte)
    cmp rdx, r13
    jnz module_loop ; if length not equal move to next item
    ; if equal compare strings

    ; rbx contains module str length
    mov rcx, [rax + 48h + 8h] ; access current module str ptr to string array
    mov rdx, r12 ; target module name as arg2
    mov r8, r13  ; length as arg3

    ;save rax for llater if needed
    push rax

    ; need a string comparisson func that handles the char offsets of module name (every other byte)
    ; rcx - module name ptr
    ; rdx - target module name
    ; r8 - common real length

    call compare_module_strings ; compare strings that handles the null bytes


    ; check if rax is 0; if not strings not equal
    cmp rax, 0
    ; restore registers
    pop rax
    jnz module_loop ; if no match continue looping 

    ; if match find the base address from restored rax

    ; once module is found, this would be to get the baseAddress member with the address
    mov rax, [rax + 20h] ; get base of found DLL
    ret

base_error:
    mov rax, 0 ; return 0 if error
    ret

GetBase endp

; essentually custom GPA specific to ntdll.dll in assembly
GetNtdllBase proc
    ;http:;www.rohitab.com/discuss/topic/45310-obtain-dll-addresses-from-the-peb-of-a-64-bit-process/
    ; Load the address of PEB into RAX
    xor rax, rax
    mov rax, gs:[60h]  ;

    ; Retrieve the base address of ntdll.dll from the PEB
    mov rax, [rax + 18h]           ; Offset of Ldr in PEB is 24 bytes
    mov rax, [rax + 20h]         ; Offset of InLoadOrderModuleList in Ldr is 48 bytes

    mov rax, [rax]                ; Get the first entry in the InLoadOrderModuleList (ntdll.dll)
    ; this returns a PLDR_ struct that can be accessed from cpp -> offset to base is supposedly x18

    mov rax, [rax + 20h]

    ; to make this able to search for DLLs, can check DLL module name and if not equal
    ; deref again and repeat
    ; deref seems to traverse the linked list


    ret
GetNtdllBase endp

; essentially a custom GMH in assembly
resolveFunction PROC
        ; Input parameters:
        ;   rcx: Pointer to functionName string
        ;   rdx: Length of the string
        ;   r8:  Base address of module
        ; Output:
        ;   rax: Length of the string
        
        ; set functionName rcx (arg1) to r9 as pointer
        mov r9, rcx         

       	; set length of string rdx (arg2) to r13 (value?)
        mov r13, rdx        

        ; pass r8 (arg3) which has module base address into r11
        mov r11, r8        

        ; pass DLL PE HEADER to resolve export table for function symbol search
        xor r8, r8                        ; Clear R8               
        mov r8d, dword ptr [r11 + 3Ch]
        mov rdx, r8                		  ; Move DOS->e_lfanew to RDX
        add rdx, r11                      ; Calculate PE Header address + base address
        ; rdx should now point to PE header
        mov r8d, dword ptr [rdx + 88h]    ; Calculate offset to the export table (OptionalHeader (0x18) + DataDirectory (0x70) offsets)
        add r8, r11                       ; Update R8 to point to the export table
        ; r8 should now point to Export Table
        sub rsi, rsi                      ; Clear RSI
        mov esi, dword ptr [r8 + 20h]     ; Calculate the offset to the names table with split offsets
        add rsi, r11                      ; Update RSI to point to the names table
        ; rsi should now point to Names Table
        mov r12, 0                        ; Initialize RCX to 0
        
        ; registers we need currently in this func:
        ; r9  ; funcName
        ; r13 ; funcName length
        ; r11 ; ntdll base
        ; rdx ; ntdll PE header address
        ; r8  ; ntdll Export table address
        ; rsi ; ntdll Names table address
        ; r12 ; as iterations counter

        ; rax will be used to point to ntdll funcs as we iterate
        ; this leaves the following registers free for other operations = rcx, rbx, rdi, r14, r15, r10     

        ; loop the different process loaded DLL symbol/function names
    Get_Function:
        inc r12                       ; Increment the ordinal
        mov rax, 0                    ; Clear RAX
        mov eax, [rsi + r12 * 4]      ; Load the next function name offset
        add rax, r11                  ; Calculate the actual function name address

        ; Extra check for currentFunc length matching the FuncName length
       
        ; save all important registers and restore after str length comparisons
        push r9
        push r13
        push r11
        push rdx
        push r8
        push rsi
        push r12
        push rax

        ; compare string length
        ; resolve length of current func str
        mov rcx, rax    ; move current func ptr to rcx
        
        call get_string_length ; this will return current func length in rax 
        ; rax is now currentFunc length and rcx is ptr to current func
        cmp rax, r13    ; compare str length between current func (rax) and funcName (r13)
        ; if not equal go to next iteration - no need to restore rax  
        
        ; restore all registers
        pop rax
        pop r12
        pop rsi
        pop r8
        pop rdx
        pop r11
        pop r13
        pop r9

        jnz Get_Function
        ; if equal, restore rax and continue comparison

        ; Perform a byte comparison between 2 strings of the same length
        ; save all important registers again and restore after str comparisons
        push r9
        push r13
        push r11
        push rdx
        push r8
        push rsi
        push r12
        push rax

        ; prep for func call here
        ; rax has our currentFunc ptr
        ; r9 is FuncName (by value if we want ptr comment out line 40)
        ; r13 is length of funcName - but same as currentFunc since there was a match earlier for length
        mov rcx, rax ; ptr to currentFunc
        mov rdx, r9  ; ptr to FuncName
        mov r8, r13  ; common length of strings

        ; call
        call compare_strings ; will return 0 for true 
        ; if not zero compare_strings will restore (r8, rdx, rcx) and loop again (Get_Function)

        ; check if rax is 0; if not strings not equal
        cmp rax, 0
        ; restore registers
        pop rax
        pop r12
        pop rsi
        pop r8
        pop rdx
        pop r11
        pop r13
        pop r9
        jnz Get_Function ; this is triggered if string compare failed 
        ; should be safe to restore registers before as long as we dont modify conditional flags

        ; if here rax is 0 and strings are the same
        ; means rax after restore is the correct currentFunc address
        ; we can now resolve the address properly?

        ; once symbol names match between currentFunc and funcName we can resolve the func address         
        sub rsi, rsi                  ; Clear RSI
        mov esi, [r8 + 20h + 4h]      ; Calculate offset to the ordinals table with split offsets
        add rsi, r11                  ; Update RSI to point to the ordinals table
        mov r12w, [rsi + r12 * 2]     ; Load ordinal number 
        sub rsi, rsi                  ; Clear RSI again
        mov esi, [r8 + 0eh + 0eh]     ; Calculate offset to the address table with split offsets
        add rsi, r11                  ; Update RSI to point to the address table
        mov rdx, 0                    ; Clear RDX
        mov edx, [rsi + r12 * 4]      ; Load the function address (offset)
        add rdx, r11                  ; Calculate the actual function address

        ; Save function address for later use     
        ; Save the actual address of the target function in getFunctionAddr
        mov rax, rdx
        ret        


resolveFunction ENDP

; ----------------------- HELPER FUNCS -----------------------

; logic to find currentFunc str length
get_string_length PROC

    ; rcx has currentFunc ptr 

    ; Initialize rdi to use it as a counter
    xor rdi, rdi

    ; Loop through the string byte by byte
count_loop:
    cmp byte ptr [rcx + rdi], 0   ; Compare the byte with null terminator
    je count_done             ; If null terminator is found, end counting
    inc rdi                   ; Increment the counter
    jmp count_loop            ; Continue counting

count_done:
    ; Move the length to rax
    mov rax, rdi
    ret ; return value goes back to rax

get_string_length ENDP

; logic to compare 2 strings via byte comparison
compare_strings PROC
    ; Inputs:
    ;   rcx: Pointer to the first string  ; currentFunc
    ;   rdx: Pointer to the second string ; FuncName (r9)
    ;   r8: Length of the strings
    ; Outputs:
    ;   rax: Zero if strings are equal, non-zero otherwise

    ; Compare byte by byte
    ; use r14 and r15 since free
    xor r14, r14
    xor r15, r15
    ; rcx is default loop counter
    mov rbx, rcx ; put rcx str into rsi
    mov rcx, r8 ; put length in rcx

compare_loop:
    mov r14b, BYTE PTR [rbx]  ; Load byte from the first string
    mov r15b, BYTE PTR [rdx]  ; Load byte from the second string ;rdx
    cmp r14b, r15b              ; Compare bytes
    jne strings_not_equal   ; If bytes are not equal, jump to strings_not_equal
    inc rbx                 ; Move to the next byte in the first string
    inc rdx                 ; Move to the next byte in the second string ; rdx
    loop compare_loop       ; Loop until all bytes are compared

    ; If loop completes without finding a difference, strings are equal
    mov rax, 0  ; Set return value to 0 (strings are equal)
    jmp end_compare_strings

; if not equal restore registers from stack and loop to next currentFunc?
strings_not_equal:
    ; If a difference is found, set return value to non-zero
    mov rax, 1  ; Set return value to 1 (strings are not equal)

end_compare_strings:
    ret

compare_strings ENDP

; module name search func
; rcx will be ptr to module name that is null byte character seperated (need to skip a byte for each comparison)
compare_module_strings PROC
    ; Inputs:
    ;   rcx: Pointer to the first string  ; current module name (need to iterate over every other byte)
    ;   rdx: Pointer to the second string ; target module name 
    ;   r8: Length of the strings
    ; Outputs:
    ;   rax: Zero if strings are equal, non-zero otherwise

    ; Compare byte by byte
    ; use r14 and r15 since free
    xor r14, r14
    xor r15, r15
    ; rcx is default loop counter
    mov rbx, rcx ; put rcx str into rsi
    mov rcx, r8 ; put length in rcx

compare_loop:
    mov r14b, BYTE PTR [rbx]  ; Load byte from the first string
    mov r15b, BYTE PTR [rdx]  ; Load byte from the second string ;rdx
    cmp r14b, r15b              ; Compare bytes
    jne strings_not_equal   ; If bytes are not equal, jump to strings_not_equal
    ; for every iteration rbx needs to interate twice
    inc rbx                 ; Move to the next byte in the first string
    inc rbx
    inc rdx                 ; Move to the next byte in the second string ; rdx
    loop compare_loop       ; Loop until all bytes are compared

    ; If loop completes without finding a difference, strings are equal
    mov rax, 0  ; Set return value to 0 (strings are equal)
    jmp end_compare_strings

; if not equal restore registers from stack and loop to next currentFunc?
strings_not_equal:
    ; If a difference is found, set return value to non-zero
    mov rax, 1  ; Set return value to 1 (strings are not equal)

end_compare_strings:
    ret

compare_module_strings ENDP

; obfuscation helpers

; custom string obfuscate
Obf PROC

	; In:
		; rcx - cleartext str pointer
		; rdx - length
	; Out:
		; rax - Obf string

	; iterate over string contents
	; for odd character index increment
	; for even character index decrement
	; this should change the byte values +1 or -1
	; e.g 0x42 ('A') may become either 0x41 (even index) or 0x43 (odd index)

	; save pointer to string 
	push rcx
    mov rbx, rcx ; move str to rbx
	xor r14, r14 ; clear out r14 to point to byte
	; r9 will be our counter
	xor r9, r9
	xor rcx, rcx
		
routine:
	; check if [rbx+r9] content is NULL 
	cmp byte ptr [rbx + r9], 0
	je done ; if null - ZF set - done 

	mov r14b, byte ptr [rbx + r9] ; current rbx byte into r14b (lower 8 bits)
	; perform odd or even check on rcx
	mov rcx, r9
	inc rcx
	
	test rcx, 1              ; Test the LSB with 1
	jz   even_number         ; Jump if the LSB is 0 (even)
	
	; Odd number code here
	inc r14b
	mov byte ptr [rbx + r9], r14b
    jmp end_check
	
even_number:
	; Even number code here
	dec r14b
	mov byte ptr [rbx + r9], r14b
	
end_check:

	; increment counter
	inc r9
	jmp routine ; go to next byte	

done:	
	pop rax ; get string pointer (hopefully now obuscated) 
	ret

Obf ENDP

; custom string deobfuscate
DeObf PROC

	; In:
		; rcx - cleartext str pointer
		; rdx - length
	; Out:
		; rax - Obf string

	; iterate over string contents
	; for odd character index decrement (inverse of Obfuscate)
	; for even character index increment (inverse of Obfuscate)
	; this should change the byte values +1 or -1
	; e.g 0x42 ('A') may become either 0x41 (even index) or 0x43 (odd index)

	; save pointer to string 
	push rcx
    mov rbx, rcx ; move str to rbx
	xor r14, r14 ; clear out r14 to point to byte
	; r9 will be our counter
	xor r9, r9
	xor rcx, rcx
		
routine:
	; check if [rbx+r9] content is NULL 
	cmp byte ptr [rbx + r9], 0
	je done ; if null - ZF set - done 

	mov r14b, byte ptr [rbx + r9] ; current rbx byte into r14b (lower 8 bits)
	; perform odd or even check on rcx
	mov rcx, r9
	inc rcx
	
	test rcx, 1              ; Test the LSB with 1
	jz   even_number         ; Jump if the LSB is 0 (even)
	
	; Odd number code here
	dec r14b
	mov byte ptr [rbx + r9], r14b
    jmp end_check
	
even_number:
	; Even number code here
	inc r14b
	mov byte ptr [rbx + r9], r14b
	
end_check:

	; increment counter
	inc r9
	jmp routine ; go to next byte	

done:	
	pop rax ; get string pointer (hopefully now cleartext) 
	ret


DeObf ENDP


END