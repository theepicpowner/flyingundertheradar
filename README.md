# FlyingUnderTheRadar

## Concept code for
- Custom GetProcAddress in assembly
- Custom GetModuleHandle in assembly
- Simple string obfuscation/deobfuscation in assembly
- Resolving string length and performing string comparisons in assembly

## See my blog for explanations on the code:
https://theepicpowner.gitlab.io/

## x64 MASM assembly
Make sure to set MASM in build dependencies of VS, as all assembly is MASM formatted.
